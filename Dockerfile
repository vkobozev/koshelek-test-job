FROM node:12

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install express --save

COPY . .

EXPOSE 3000
CMD [ "node", "app.js" ]