const express = require('express');
const server = express();

const hostname = '0.0.0.0';
const port = 3000;

server.use(express.static("static"));

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});